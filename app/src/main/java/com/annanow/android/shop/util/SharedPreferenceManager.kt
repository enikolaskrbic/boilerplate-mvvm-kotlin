package com.annanow.android.shop.util

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class SharedPreferenceManager constructor(private val sharedPreferences: SharedPreferences) {
    private val TOKEN = "TOKEN"
    private val LANGUAGE = "LANGUAGE"

    var token: String
        get() = sharedPreferences.getString(TOKEN, "")
        set(googleToken) = sharedPreferences.edit().putString(TOKEN, googleToken).apply()

    var language: String
        get() = sharedPreferences.getString(LANGUAGE, "en")
        set(language) = sharedPreferences.edit().putString(LANGUAGE, language).apply()

}