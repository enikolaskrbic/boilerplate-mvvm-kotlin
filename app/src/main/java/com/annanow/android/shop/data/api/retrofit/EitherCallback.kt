package com.annanow.android.shop.data.api.retrofit

import com.annanow.android.shop.data.api.retrofit.annotation.InvocationPolicy

interface EitherCallback<L, R> {

    /**
     * Called if the response status code is in [InvocationPolicy.left] or if [InvocationPolicy.left] is
     * empty, status code is in range [InvocationPolicy.leftRange]. `left` might be `null` as a
     * result of unexpected data from the server and hence a conversion error.
     *
     * @param left The first possible response.
     *
     * @see InvocationPolicy.left
     * @see InvocationPolicy.leftRange
     */
    fun onLeft(left: L)

    /**
     * Called if the response status code is in [InvocationPolicy.right] or if [InvocationPolicy.right]
     * is empty, status code is in range [InvocationPolicy.rightRange]. `right` might be `null` as a
     * result of unexpected data from the server and hence a conversion error.
     *
     * @param right The second possible response.
     *
     * @see InvocationPolicy.right
     * @see InvocationPolicy.rightRange
     */
    fun onRight(right: R)

    /**
     * Called if the response status code is not found in the [InvocationPolicy.left], [ ][InvocationPolicy.right], [InvocationPolicy.leftRange], [InvocationPolicy.rightRange] or when
     * any exception occurred during the execution of the request or the processing of the response.
     *
     * @param t The exception occurred.
     */
    fun onException(t: Throwable)
}