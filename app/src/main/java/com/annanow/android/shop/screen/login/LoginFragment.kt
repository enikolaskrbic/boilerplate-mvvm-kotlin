package com.annanow.android.shop.screen.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.annanow.android.shop.R
import com.annanow.android.shop.base.BaseFragment
import com.annanow.android.shop.data.event.ErrorEvent
import com.annanow.android.shop.data.event.LoadingEvent
import com.annanow.android.shop.data.event.SuccessEvent
import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.data.usecase.value.SignInValue
import com.annanow.android.shop.error.AnnanowError
import com.annanow.android.shop.error.EmailValidationError
import com.annanow.android.shop.error.PasswordValidationError
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment() {

    private lateinit var mLoginViewModel: LoginViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mLoginViewModel = ViewModelProviders.of(this, mViewModelFactory).get(LoginViewModel::class.java)
        initListener()
        observe()
    }

    private fun observe() {
        mLoginViewModel.signInEvent.observe(this, Observer {
            when (it) {
                is SuccessEvent<UserModel> -> {
                    showToast("SuccessEvent")
                }
                is LoadingEvent<UserModel> -> {
                    showToast("LoadingEvent")
                }
                is ErrorEvent<UserModel> -> {
                    showError(it.error)
                }
            }
        })
    }

    private fun showError(error: AnnanowError) {
        when(error){
            is EmailValidationError -> {
                showToast(error.resourceId)
            }
            is PasswordValidationError -> {
                showToast(error.resourceId)
            }
            else -> {
                showToast("Unknown")
            }
        }
    }

    private fun initListener() {
        button_login.setOnClickListener {
            mLoginViewModel.signIn(SignInValue(edittext_username.text.toString(), edittext_password.text.toString()))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }
}