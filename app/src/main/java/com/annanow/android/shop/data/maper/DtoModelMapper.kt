package com.annanow.android.shop.data.maper

import com.annanow.android.shop.data.api.dto.response.SignInResponse
import com.annanow.android.shop.data.repository.model.UserModel


object DtoModelMapper {
    fun mapUser(signInResponse: SignInResponse): UserModel {
        return UserModel(signInResponse.id, signInResponse.email)
    }
}