package com.annanow.android.shop.di.module

import android.app.Application
import com.annanow.android.shop.BuildConfig
import com.annanow.android.shop.data.api.AnnanowApi
import com.annanow.android.shop.data.api.AnnanowInterceptor
import com.annanow.android.shop.data.api.retrofit.EitherCallAdapterFactory
import com.annanow.android.shop.util.SharedPreferenceManager
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    @Singleton
    fun provideHttpCatche(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideFirebaseInterceptorCatche(sharedPreferenceManager: SharedPreferenceManager): AnnanowInterceptor {
        return AnnanowInterceptor(sharedPreferenceManager)
    }

    @Provides
    @Singleton
    fun provideStethoInterceptorCatche(): StethoInterceptor {
        return StethoInterceptor()
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    internal fun providesApiOkHttpBuilder(cache: Cache, annanowInterceptor: AnnanowInterceptor, stethoInterceptor: StethoInterceptor): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.cache(cache)
        clientBuilder.addNetworkInterceptor(annanowInterceptor)
        if (BuildConfig.DEBUG) {
            clientBuilder.addNetworkInterceptor(stethoInterceptor)
        }
        clientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        clientBuilder.readTimeout(30, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(30, TimeUnit.SECONDS)

        return clientBuilder.build()
    }

    @Singleton
    @Provides
    internal fun provideAnnanowRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return getRetrofit(BuildConfig.BASE_URL, gson, okHttpClient)
    }

    private fun getRetrofit(baseUrl: String, gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(EitherCallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideAnnanowApiServices(retrofit: Retrofit): AnnanowApi {
        return retrofit.create<AnnanowApi>(AnnanowApi::class.java)
    }

}