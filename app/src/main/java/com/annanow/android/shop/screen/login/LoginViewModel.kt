package com.annanow.android.shop.screen.login

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.annanow.android.shop.data.usecase.SignInUseCase
import com.annanow.android.shop.data.usecase.value.SignInValue
import com.annanow.android.shop.testing.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class LoginViewModel @Inject constructor(private val signInUseCase: SignInUseCase) : ViewModel() {

    private val _signInValue = MutableLiveData<SignInValue>()
    val signInEvent = Transformations.switchMap(_signInValue) { signInUseCase.invoke(it) }

    fun signIn(signInValue: SignInValue) {
        _signInValue.postValue(signInValue)
    }
}