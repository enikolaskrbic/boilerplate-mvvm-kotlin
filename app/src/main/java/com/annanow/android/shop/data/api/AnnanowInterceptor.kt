package com.annanow.android.shop.data.api

import android.util.Log
import com.annanow.android.shop.util.SharedPreferenceManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import okio.Buffer
import java.io.IOException
import javax.inject.Inject

class AnnanowInterceptor @Inject constructor(val sharedPreferenceManager: SharedPreferenceManager) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        request = request.newBuilder()
                .addHeader("Application", "SHOP")
                .addHeader("Accept-Language", sharedPreferenceManager.language)
                .addHeader("Content-Type", "application/json").build()

        val t1 = System.nanoTime()

        request.body()?.run {
            if (contentLength() < 2048)
                Log.d("Retrofit", String.format("Sending request %s on %s%n%s", request.url(), chain
                        .connection(), request.headers()) + " Params " + bodyToString(request))
        }

        val response = chain.proceed(request)

        val responseBodyString = response?.body()?.string()
        val t2 = System.nanoTime()
        Log.d("Retrofit", String.format("Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6, response.headers()) + "Body " + responseBodyString)

        return response.newBuilder().body(ResponseBody.create(response.body()?.contentType(),
                responseBodyString.orEmpty())).build()
    }

    private fun bodyToString(request: Request): String {
        return try {
            val buffer = Buffer()
            request.body()?.writeTo(buffer)
            buffer.readUtf8()
        } catch (e: IOException) {
            "did not work"
        } catch (e: NullPointerException) {
            "did not work nullPointer"
        } catch (e: OutOfMemoryError) {
            "OutOfMemoryError "
        }

    }
}