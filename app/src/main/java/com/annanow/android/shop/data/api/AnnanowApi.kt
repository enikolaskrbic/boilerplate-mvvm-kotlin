package com.annanow.android.shop.data.api

import com.annanow.android.shop.data.api.dto.request.SignInRequest
import com.annanow.android.shop.data.api.dto.response.SignInResponse
import com.annanow.android.shop.data.api.retrofit.EitherCall
import com.annanow.android.shop.error.ErrorResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface AnnanowApi {
    @POST("authentication/shop/sign-in")
    fun signIn(@Body shop: SignInRequest): EitherCall<SignInResponse, ErrorResponse>
}