package com.annanow.android.shop.screen.splash

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.annanow.android.shop.base.BaseFragment
import com.annanow.android.shop.screen.login.LoginViewModel

class SpalshFragment: BaseFragment() {
    private lateinit var mLoginViewModel: LoginViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mLoginViewModel = ViewModelProviders.of(this, mViewModelFactory).get(LoginViewModel::class.java)
    }
}