package com.annanow.android.shop.di

import android.app.Application
import com.annanow.android.shop.AnnanowShopApp
import com.annanow.android.shop.di.module.ActivityBindingModule
import com.annanow.android.shop.di.module.AppModule
import com.annanow.android.shop.di.module.ServiceModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    (AppModule::class),
    (ActivityBindingModule::class),
    (AndroidInjectionModule::class),
    (ServiceModule::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent

    }

    fun inject(annanowShopApp: AnnanowShopApp)

}