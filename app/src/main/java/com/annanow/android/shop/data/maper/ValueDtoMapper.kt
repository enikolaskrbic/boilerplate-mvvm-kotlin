package com.annanow.android.shop.data.maper

import com.annanow.android.shop.data.api.dto.request.SignInRequest
import com.annanow.android.shop.data.usecase.value.SignInValue

object ValueDtoMapper {
    fun mapSignIn(signInValue: SignInValue): SignInRequest {
        return SignInRequest(signInValue.email,
                signInValue.password)
    }
}