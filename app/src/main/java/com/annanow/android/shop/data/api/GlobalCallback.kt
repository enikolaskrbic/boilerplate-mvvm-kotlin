package com.annanow.android.shop.data.api

import android.arch.lifecycle.MutableLiveData
import com.annanow.android.shop.data.api.retrofit.EitherCallback
import com.annanow.android.shop.data.event.ErrorEvent
import com.annanow.android.shop.data.event.Event
import com.annanow.android.shop.error.BackendError
import com.annanow.android.shop.error.ErrorResponse
import com.annanow.android.shop.error.ThrowableError

class GlobalCallback<L, R, T>(private val liveData: MutableLiveData<Event<T>>, val onSuccess: (L) -> Unit) : EitherCallback<L, R> {

    override fun onLeft(left: L) {
        onSuccess(left)
    }

    override fun onRight(right: R) {
        if (right is ErrorResponse)
            liveData.postValue(ErrorEvent(BackendError(right)))
    }

    override fun onException(t: Throwable) {
        liveData.postValue(ErrorEvent(ThrowableError(t)))
    }
}