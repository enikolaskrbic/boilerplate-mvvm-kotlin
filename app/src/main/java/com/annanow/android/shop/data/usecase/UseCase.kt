package com.annanow.android.shop.data.usecase

interface UseCase<T, F> {
    fun invoke(value: T): F
}