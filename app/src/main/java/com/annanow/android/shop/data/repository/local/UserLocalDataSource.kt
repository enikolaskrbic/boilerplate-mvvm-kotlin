package com.annanow.android.shop.data.repository.local

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.annanow.android.shop.data.event.Event
import com.annanow.android.shop.data.maper.ModelRoomMapper
import com.annanow.android.shop.data.repository.UserDataSource
import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.data.room.UserDao
import com.annanow.android.shop.data.usecase.value.SignInValue
import com.annanow.android.shop.util.SharedPreferenceManager
import org.jetbrains.anko.doAsync
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserLocalDataSource @Inject constructor(private val mContext: Context,
                                              private val mPreferenceManager: SharedPreferenceManager,
                                              private val userDao: UserDao) : UserDataSource {
    override fun save(userModel: UserModel) {
        doAsync {
            userDao.save(ModelRoomMapper.mapUser(userModel))
        }
    }

    override fun signIn(signInValue: SignInValue): LiveData<Event<UserModel>> {
        return MutableLiveData<Event<UserModel>>()
    }
}