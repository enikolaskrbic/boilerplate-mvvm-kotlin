package com.annanow.android.shop.data.repository

import android.arch.lifecycle.LiveData
import com.annanow.android.shop.data.event.Event
import com.annanow.android.shop.data.event.SuccessEvent
import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.data.usecase.value.SignInValue
import com.annanow.android.shop.di.annotation.Local
import com.annanow.android.shopdi.annotation.Remote
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(@Remote private var mRemoteUserDataSource: UserDataSource,
                                         @Local private var mLocalUserDataSource: UserDataSource) : UserDataSource {


    override fun save(userModel: UserModel) {
        mLocalUserDataSource.save(userModel)
    }

    override fun signIn(signInValue: SignInValue): LiveData<Event<UserModel>> {
        val signInResponse = mRemoteUserDataSource.signIn(signInValue)
        signInResponse.observeForever {
            when (it) {
                is SuccessEvent -> {
                    it.data?.run { save(it.data) }
                }
            }
        }
        return signInResponse
    }
}