package com.annanow.android.shop.di.module

import com.annanow.android.shop.screen.login.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeGoogleAuthFragment(): LoginFragment

}
