package com.annanow.android.shop.di.module

import com.annanow.android.shop.service.firebase.MyFirebaseMessagingService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ServiceModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMyFirebaseService(): MyFirebaseMessagingService

}