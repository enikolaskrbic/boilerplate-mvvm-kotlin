package com.annanow.android.shop.error

sealed class AnnanowError
//GLOBAL ERROR
object Unknown : AnnanowError()
//API ERROR
data class ThrowableError(val throwable: Throwable) : AnnanowError()
data class BackendError(val error: ErrorResponse) : AnnanowError()
//VIEW ERROR
data class EmailValidationError(val resourceId : Int) : AnnanowError()
data class PasswordValidationError(val resourceId : Int) : AnnanowError()