package com.annanow.android.shop.data.usecase

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.annanow.android.shop.data.event.ErrorEvent
import com.annanow.android.shop.data.event.Event
import com.annanow.android.shop.data.repository.UserRepository
import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.data.usecase.value.SignInValue
import com.annanow.android.shop.util.Validator
import javax.inject.Inject

class SignInUseCase @Inject constructor(private val userRepository: UserRepository) : UseCase<SignInValue, LiveData<Event<UserModel>>> {
    override fun invoke(value: SignInValue): LiveData<Event<UserModel>> {
        Validator.validateEmail(value.email)?.run {
            val  retValue = MutableLiveData<Event<UserModel>>()
            retValue.postValue(ErrorEvent(this))
            return retValue
        }
        Validator.validatePassword(value.password)?.run {
            val  retValue = MutableLiveData<Event<UserModel>>()
            retValue.postValue(ErrorEvent(this))
            return retValue
        }
        return userRepository.signIn(value)
    }
}