package com.annanow.android.shop.data.usecase.value

data class SignInValue(val email: String, val password: String)