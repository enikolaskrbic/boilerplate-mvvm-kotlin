package com.annanow.android.shop.data.repository

import android.arch.lifecycle.LiveData
import com.annanow.android.shop.data.event.Event
import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.data.usecase.value.SignInValue

interface UserDataSource {
    fun signIn(signInValue: SignInValue) : LiveData<Event<UserModel>>
    fun save(userModel: UserModel)
}