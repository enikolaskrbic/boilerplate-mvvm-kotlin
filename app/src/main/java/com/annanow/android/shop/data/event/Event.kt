package com.annanow.android.shop.data.event

import com.annanow.android.shop.error.AnnanowError

sealed class Event<T>
data class SuccessEvent<T>(val data: T?) : Event<T>()
class LoadingEvent<T> : Event<T>()
data class ErrorEvent<T>(val error: AnnanowError): Event<T>()