package com.annanow.android.shop.data.api.retrofit

import android.os.Handler
import com.annanow.android.shop.data.api.retrofit.annotation.InvocationPolicy
import com.annanow.android.shop.data.api.retrofit.annotation.InvocationPolicy.StatusCodeRange.Companion.inRange
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response

@Suppress("UNCHECKED_CAST")
class EitherCall<L, R>(private val call: Call<ResponseBody>,
                       private val leftConverter: Converter<ResponseBody, L>,
                       private val rightConverter: Converter<ResponseBody, R>,
                       val invocationPolicy: InvocationPolicy,
                       private val handler: Handler) {
    private var callback: EitherCallback<L, R>? = null
    private var converterExc: Boolean = false

    init {
        checkEmptyBounds()
    }

    private fun checkEmptyBounds() {
        if (invocationPolicy.right().isEmpty() &&
                invocationPolicy.left().isEmpty() &&
                invocationPolicy.leftRange().isEmpty() &&
                invocationPolicy.rightRange().isEmpty()) {

            throw IllegalStateException("Invocation policy has no bound for status code checking.")
        }
    }

    fun callback(callback: EitherCallback<L, R>) {
        this.callback = callback

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val code = response.code()

                if (contains(invocationPolicy.left(), code)) {
                    callOnLeft(response)
                    return
                } else if (contains(invocationPolicy.right(), code)) {
                    callOnRight(response)
                    return
                }

                val leftEmpty = invocationPolicy.left().isEmpty()
                val rightEmpty = invocationPolicy.right().isEmpty()

                if (!leftEmpty && !rightEmpty) {
                    callback.onException(IllegalStateException("Either left nor right does not contain response" +
                            " status code: " + code))
                    return
                }

                if (leftEmpty && inRange(invocationPolicy.leftRange(), code)) {
                    callOnLeft(response)
                } else if (rightEmpty && inRange(invocationPolicy.rightRange(), code)) {
                    callOnRight(response)
                } else {
                    callback.onException(IllegalStateException("Cannot determine status code: $code"))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback.onException(t)
            }
        })
    }

    private fun callOnRight(response: Response<ResponseBody>) {
        val right = convertRight(response)
        if (converterExc) {
            return
        }

        try {
            handler.post { callback?.onRight(right) }
        } catch (e: Exception) {
            callback?.onRight(right)
        }

    }

    private fun callOnLeft(response: Response<ResponseBody>) {
        val left = convertLeft(response)
        if (converterExc) {
            return
        }

        try {
            handler.post { callback?.onLeft(left) }
        } catch (e: Exception) {
            callback?.onLeft(left)
        }

    }

    private fun clientOrServerError(code: Int): Boolean {
        return inRange(InvocationPolicy.StatusCodeRange.CLIENT_ERROR, code) || inRange(InvocationPolicy.StatusCodeRange.SERVER_ERROR, code)
    }

    private fun convertRight(response: Response<ResponseBody>): R {
        return convert(rightConverter, determineResponseBody(response)) as R
    }

    private fun convertLeft(response: Response<ResponseBody>): L {
        return convert(leftConverter, determineResponseBody(response)) as L
    }

    private fun determineResponseBody(response: Response<ResponseBody>): ResponseBody? {
        return if (clientOrServerError(response.code())) response.errorBody() else response.body()
    }

    private fun convert(converter: Converter<ResponseBody, *>, body: ResponseBody?): Any? {
        if (body == null || body.contentLength() == 0L) {
            return null
        }

        try {
            return converter.convert(body)
        } catch (e: Exception) {
            converterExc = true
            callback?.onException(e)
        }

        return null
    }

    private fun contains(statusCodes: IntArray?, search: Int): Boolean {
        if (statusCodes == null || statusCodes.isEmpty()) {
            return false
        }

        for (statusCode in statusCodes) {
            if (statusCode == search) {
                return true
            }
        }

        return false
    }

}