package com.annanow.android.shop.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.annanow.android.shop.data.room.model.User

@Database(entities = [User::class], version = 1)
@TypeConverters(Converters::class)
abstract class AnnanowDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}