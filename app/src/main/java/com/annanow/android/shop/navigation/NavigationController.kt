package com.annanow.android.shop.navigation

import com.annanow.android.shop.R
import com.annanow.android.shop.HomeActivity
import com.annanow.android.shop.screen.login.LoginFragment
import javax.inject.Inject

class NavigationController @Inject constructor(homeActivity: HomeActivity) {

    private val containerId = R.id.container
    private val fragmentManager = homeActivity.supportFragmentManager

    fun navigateToLogin() {
        fragmentManager.beginTransaction()
                .replace(containerId, LoginFragment())
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

}