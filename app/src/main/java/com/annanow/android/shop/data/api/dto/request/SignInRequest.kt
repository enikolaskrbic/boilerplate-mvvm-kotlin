package com.annanow.android.shop.data.api.dto.request

data class SignInRequest(val email:String, val password: String)