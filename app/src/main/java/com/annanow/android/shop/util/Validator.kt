package com.annanow.android.shop.util

import com.annanow.android.shop.R
import com.annanow.android.shop.error.EmailValidationError
import com.annanow.android.shop.error.PasswordValidationError

object Validator {

    fun validateEmail(email: String): EmailValidationError? {
        if(email.trim().isEmpty())
            return EmailValidationError(R.string.error_email_must_not_be_null)
        return null
    }

    fun validatePassword(password: String): PasswordValidationError? {
        if(password.trim().isEmpty())
            return PasswordValidationError(R.string.error_password_must_not_be_null)
        return null
    }
}