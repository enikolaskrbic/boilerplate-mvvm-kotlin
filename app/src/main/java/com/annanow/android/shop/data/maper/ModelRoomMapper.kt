package com.annanow.android.shop.data.maper

import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.data.room.model.User

object ModelRoomMapper {
    fun mapUser(userModel: UserModel): User {
        return User(userModel.id, userModel.email)
    }
}