package com.annanow.android.shop.base

import android.arch.lifecycle.ViewModelProvider
import android.support.v4.app.Fragment
import android.widget.Toast
import com.annanow.android.shop.di.Injectable
import javax.inject.Inject

open class BaseFragment : Fragment(), Injectable {

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(resourceId: Int) {
        context?.run {
            Toast.makeText(context, this.getString(resourceId), Toast.LENGTH_SHORT).show()
        }
    }

}