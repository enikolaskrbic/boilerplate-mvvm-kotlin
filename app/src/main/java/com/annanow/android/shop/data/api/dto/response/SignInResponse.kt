package com.annanow.android.shop.data.api.dto.response

import java.util.*

data class SignInResponse(
        val accessToken: String,
        val apiKey: String,
        val countryId: Long,
        val email: String,
        val firstName: String,
        val hasTemporaryPasswordExpired: Boolean,
        val hasWorkScheduleDefault: Boolean,
        val id: Long,
        val isAcceptedTermsAndConditions: Boolean,
        val isDeleted: Boolean,
        val isFirstTime: Boolean,
        val isPluginEnabled: Boolean,
        val isSpecial: Boolean,
        val isTemporaryPassword: Boolean,
        val languageCode: String,
        val lastName: String,
        val partnerId: Long,
        val phoneNumber: String,
        val profilePicturePath: String,
        val resetPasswordCode: String,
        val role: String,
        val shopName: String,
        val storeId: Long,
        val storeStatus: String,
        val switchedOrderAllowed: Boolean,
        val timestamp: Date
)