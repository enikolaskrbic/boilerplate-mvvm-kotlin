package com.annanow.android.shop.data.repository.remote

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.annanow.android.shop.data.api.AnnanowApi
import com.annanow.android.shop.data.api.GlobalCallback
import com.annanow.android.shop.data.event.Event
import com.annanow.android.shop.data.event.LoadingEvent
import com.annanow.android.shop.data.event.SuccessEvent
import com.annanow.android.shop.data.maper.DtoModelMapper
import com.annanow.android.shop.data.maper.ValueDtoMapper
import com.annanow.android.shop.data.repository.UserDataSource
import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.data.usecase.value.SignInValue
import com.annanow.android.shop.util.SharedPreferenceManager
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(private val mAnnanowApi: AnnanowApi,
                                               private val mPreferenceManager: SharedPreferenceManager) : UserDataSource {
    override fun save(userModel: UserModel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun signIn(signInValue: SignInValue): LiveData<Event<UserModel>> {
        val userModelLiveData = MutableLiveData<Event<UserModel>>()
        userModelLiveData.postValue(LoadingEvent())
        mAnnanowApi.signIn(ValueDtoMapper.mapSignIn(signInValue)).callback(GlobalCallback(userModelLiveData) {
            userModelLiveData.postValue(SuccessEvent(DtoModelMapper.mapUser(it)))
        })
        return userModelLiveData
    }
}