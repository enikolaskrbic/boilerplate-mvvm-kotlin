package com.annanow.android.shop.error

data class ErrorResponse(
        var errorCodes: List<String>,
        var message: String,
        private val data: ErrorData?
)

data class ErrorData(var dialogType: String?,
                     var info: DialogTypeInfo?)

data class DialogTypeInfo(
        var mailTo: String?,
        var subject: String?,
        var plainTextContent: String?
)