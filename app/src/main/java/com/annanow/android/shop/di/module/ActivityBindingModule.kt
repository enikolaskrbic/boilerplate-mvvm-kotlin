package com.annanow.android.shop.di.module

import com.annanow.android.shop.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = [(FragmentBuildersModule::class)])
    abstract fun homeActivity(): HomeActivity
}