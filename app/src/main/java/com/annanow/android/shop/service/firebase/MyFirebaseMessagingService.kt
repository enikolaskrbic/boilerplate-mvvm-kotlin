package com.annanow.android.shop.service.firebase

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.android.AndroidInjection

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        p0?.run {
            //TODO UPDATE TOKEN
        }
    }

    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)
        Log.d("", "From: " + p0?.from)
        p0?.data?.get("messageType")?.run {
            handleMessage(this, p0.data)
        }
    }

    private fun handleMessage(messageType: String, data: Map<String, String>) {
        //TODO HANDLE MESSAGE
    }
}