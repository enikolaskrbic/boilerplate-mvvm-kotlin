package com.annanow.android.shop.data.repository.model

data class UserModel(var id: Long = 0, var email: String) : Model()