package com.annanow.android.shop.di

import android.app.Application
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import com.annanow.android.shop.data.api.AnnanowApi
import com.annanow.android.shop.data.repository.UserDataSource
import com.annanow.android.shop.data.repository.local.UserLocalDataSource
import com.annanow.android.shop.data.repository.remote.UserRemoteDataSource
import com.annanow.android.shop.data.room.AnnanowDatabase
import com.annanow.android.shop.data.room.UserDao
import com.annanow.android.shop.di.annotation.Local
import com.annanow.android.shop.util.SharedPreferenceManager
import com.annanow.android.shopdi.annotation.Remote
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoTestModule {

    @Singleton
    @Provides
    @Remote
    fun bindUserRemoteDataSource(annanowApi: AnnanowApi, preferenceManager: SharedPreferenceManager): UserDataSource {
        return UserRemoteDataSource(annanowApi, preferenceManager)
    }

    @Singleton
    @Provides
    @Local
    fun bindUserLocalDataSource(application: Application, preferenceManager: SharedPreferenceManager, userDao: UserDao): UserDataSource {
        return UserLocalDataSource(application, preferenceManager, userDao)
    }


    @Singleton
    @Provides
    fun provideChatDataBaseRoom(application: Application): AnnanowDatabase {
        return Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AnnanowDatabase::class.java).allowMainThreadQueries().build()
    }

    @Singleton
    @Provides
    fun provideUserDao(annanowDatabase: AnnanowDatabase): UserDao {
        return annanowDatabase.userDao()
    }
}