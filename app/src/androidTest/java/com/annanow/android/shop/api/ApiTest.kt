package com.annanow.android.shop.api

import android.arch.core.executor.testing.CountingTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.annanow.android.shop.HomeActivity
import com.annanow.android.shop.api.simulate.AnnanowServiceFailure
import com.annanow.android.shop.api.simulate.AnnanowServiceSuccess
import com.annanow.android.shop.data.api.AnnanowApi
import com.annanow.android.shop.data.api.dto.request.SignInRequest
import com.annanow.android.shop.data.api.dto.response.SignInResponse
import com.annanow.android.shop.data.api.retrofit.EitherCallAdapterFactory
import com.annanow.android.shop.data.api.retrofit.EitherCallback
import com.annanow.android.shop.data.event.ErrorEvent
import com.annanow.android.shop.data.event.Event
import com.annanow.android.shop.data.event.SuccessEvent
import com.annanow.android.shop.data.maper.DtoModelMapper
import com.annanow.android.shop.data.repository.model.UserModel
import com.annanow.android.shop.error.BackendError
import com.annanow.android.shop.error.ErrorResponse
import com.annanow.android.shop.error.ThrowableError
import com.annanow.android.shop.util.TaskExecutorWithIdlingResourceRule
import com.annanow.android.shop.util.blockingObserve
import com.squareup.okhttp.mockwebserver.MockWebServer
import okhttp3.OkHttpClient
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

@RunWith(AndroidJUnit4::class)
class ApiTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(HomeActivity::class.java, true, true)
    @Rule
    @JvmField
    val executorRule = TaskExecutorWithIdlingResourceRule()
    @Rule
    @JvmField
    val countingAppExecutors = CountingTaskExecutorRule()

    lateinit var mMockWebServer: MockWebServer
    lateinit var mMockRetrofit: MockRetrofit
    lateinit var mRetrofit: Retrofit


    @Before
    fun setup() {
        mMockWebServer = MockWebServer()
        mRetrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(EitherCallAdapterFactory.create())
                .baseUrl(mMockWebServer.url("api/someurl/").toString())
                .client(OkHttpClient())
                .build()
        val behavior = NetworkBehavior.create()
        mMockRetrofit = MockRetrofit.Builder(mRetrofit)
                .networkBehavior(behavior)
                .build()
    }

    @Test
    fun successAspiCall() {
        val delegate = mMockRetrofit.create(AnnanowApi::class.java)
        val annanowApiSuccess = AnnanowServiceSuccess(delegate)
        val mutableLiveResponse = MutableLiveData<Event<UserModel>>()
        val ret = annanowApiSuccess.signIn(SignInRequest("neko", "neko"))
        ret.callback(object : EitherCallback<SignInResponse, ErrorResponse> {
            override fun onLeft(left: SignInResponse) {
                mutableLiveResponse.postValue(SuccessEvent(DtoModelMapper.mapUser(left)))
            }

            override fun onRight(right: ErrorResponse) {
                mutableLiveResponse.postValue(ErrorEvent(BackendError(right)))
            }

            override fun onException(t: Throwable) {
                mutableLiveResponse.postValue(ErrorEvent(ThrowableError(t)))
            }
        })

        mutableLiveResponse.blockingObserve()?.run {
            Assert.assertTrue(this is SuccessEvent)
        }
    }

    @Test
    fun failureApiCall() {
        val delegate = mMockRetrofit.create(AnnanowApi::class.java)
        val annanowApiFailure = AnnanowServiceFailure(delegate)
        val mutableLiveResponse = MutableLiveData<Event<UserModel>>()
        val ret = annanowApiFailure.signIn(SignInRequest("neko", "neko"))
        ret.callback(object : EitherCallback<SignInResponse, ErrorResponse> {
            override fun onLeft(left: SignInResponse) {
                mutableLiveResponse.postValue(SuccessEvent(DtoModelMapper.mapUser(left)))
            }

            override fun onRight(right: ErrorResponse) {
                mutableLiveResponse.postValue(ErrorEvent(BackendError(right)))
            }

            override fun onException(t: Throwable) {
                mutableLiveResponse.postValue(ErrorEvent(ThrowableError(t)))
            }
        })

        mutableLiveResponse.blockingObserve()?.run {
            Assert.assertTrue(this is ErrorEvent)
        }
    }

}