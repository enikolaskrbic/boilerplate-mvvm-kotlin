package com.annanow.android.shop.api.simulate

import com.annanow.android.shop.data.api.AnnanowApi
import com.annanow.android.shop.data.api.dto.request.SignInRequest
import com.annanow.android.shop.data.api.dto.response.SignInResponse
import com.annanow.android.shop.data.api.retrofit.EitherCall
import com.annanow.android.shop.error.ErrorResponse
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls


class AnnanowServiceFailure(private val delegate: BehaviorDelegate<AnnanowApi>) : AnnanowApi {
    override fun signIn(shop: SignInRequest): EitherCall<SignInResponse, ErrorResponse> {
        val response = Response.error<ErrorResponse>(401, ResponseBody.create(MediaType.parse("application/json"), "{\n" +
                "  \"message\": \"Angaben sind ungültig.\",\n" +
                "  \"data\": null\n" +
                "}"))
        return delegate.returning(Calls.response(response)).signIn(SignInRequest("neko","neko"))
    }
}