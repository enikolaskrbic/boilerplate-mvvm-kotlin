package com.annanow.android.shop.api.simulate

import com.annanow.android.shop.data.api.AnnanowApi
import com.annanow.android.shop.data.api.dto.request.SignInRequest
import com.annanow.android.shop.data.api.dto.response.SignInResponse
import com.annanow.android.shop.data.api.retrofit.EitherCall
import com.annanow.android.shop.error.ErrorResponse
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls


class AnnanowServiceSuccess(private val delegate: BehaviorDelegate<AnnanowApi>) : AnnanowApi {
    override fun signIn(shop: SignInRequest): EitherCall<SignInResponse, ErrorResponse> {
        val response = Response.success(ResponseBody.create(MediaType.parse("application/json"), "{\n" +
                "  \"accessToken\": \"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyNDExIiwiYXV0aCI6IlJPTEVfQlJBTkNIX0FETUlOIiwiZXhwIjoxNTM5ODQzOTA2fQ.CUbk7oIu1gWq-3WPJWWjvYcNZXSMWdWueh91g2q1Q5mXRTw8014tqYk1qXM-SG4lH-5IMzxeezU6Gz3eaZj6XQ\",\n" +
                "  \"id\": 2411,\n" +
                "  \"firstName\": \"Novosadski\",\n" +
                "  \"lastName\": \"Shop Main Edited Test Cole\",\n" +
                "  \"profilePicturePath\": null,\n" +
                "  \"phoneNumber\": \"+3816122738392\",\n" +
                "  \"countryId\": 190,\n" +
                "  \"isDeleted\": false,\n" +
                "  \"timestamp\": \"2018-01-25T09:50:32Z\",\n" +
                "  \"role\": \"ROLE_BRANCH_ADMIN\",\n" +
                "  \"email\": \"shopns.annanow@gmail.com\",\n" +
                "  \"partnerId\": null,\n" +
                "  \"storeId\": 575,\n" +
                "  \"shopName\": \"Shop NS #1\",\n" +
                "  \"storeStatus\": \"APPROVED\",\n" +
                "  \"isSpecial\": false,\n" +
                "  \"isAcceptedTermsAndConditions\": true,\n" +
                "  \"isTemporaryPassword\": false,\n" +
                "  \"hasTemporaryPasswordExpired\": null,\n" +
                "  \"hasWorkScheduleDefault\": true,\n" +
                "  \"isFirstTime\": false,\n" +
                "  \"resetPasswordCode\": null,\n" +
                "  \"apiKey\": \"rA9ftHTUdjC1alH5KtHfqxKLdbtuIJFg\",\n" +
                "  \"languageCode\": \"en\",\n" +
                "  \"isPluginEnabled\": true,\n" +
                "  \"switchedOrderAllowed\": true\n" +
                "}"))
        return delegate.returning(Calls.response(response)).signIn(SignInRequest("neko","neko"))
    }
}