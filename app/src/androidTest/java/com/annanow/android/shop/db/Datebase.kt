package com.annanow.android.shop.db

import android.arch.core.executor.testing.CountingTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import com.annanow.android.shop.data.room.AnnanowDatabase
import org.junit.After
import org.junit.Before
import org.junit.Rule
import java.io.IOException
import java.util.concurrent.TimeUnit

abstract class Datebase {
    protected lateinit var mDb: AnnanowDatabase

    @Rule
    @JvmField
    val countingTaskExecutorRule = CountingTaskExecutorRule()

    @Before
    fun onBefore() {
        mDb = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AnnanowDatabase::class.java).allowMainThreadQueries().build()
    }

    @After
    @Throws(IOException::class)
    fun onAfter() {
        countingTaskExecutorRule.drainTasks(10, TimeUnit.SECONDS)
        mDb.close()
    }
}