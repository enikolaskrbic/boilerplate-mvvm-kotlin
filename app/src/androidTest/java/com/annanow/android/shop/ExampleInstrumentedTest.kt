package com.annanow.android.shop

import android.arch.core.executor.testing.CountingTaskExecutorRule
import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.annanow.android.shop.screen.login.LoginViewModel
import com.annanow.android.shop.util.TaskExecutorWithIdlingResourceRule
import com.squareup.okhttp.mockwebserver.MockWebServer
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(HomeActivity::class.java, true, true)
    @Rule
    @JvmField
    val executorRule = TaskExecutorWithIdlingResourceRule()
    @Rule
    @JvmField
    val countingAppExecutors = CountingTaskExecutorRule()

    private lateinit var loginViewModel: LoginViewModel

    lateinit var mMockWebServer: MockWebServer
    lateinit var mMockRetrofit: MockRetrofit
    lateinit var mRetrofit: Retrofit

    @Test
    fun useAppContext() {
        // Context of the app under test.
        loginViewModel = mock(LoginViewModel::class.java)
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.annanow.android.shop.dev" , appContext.packageName)
    }
}
