package com.annanow.android.shop.util.recycleview

import android.support.annotation.IdRes
import android.support.test.espresso.PerformException
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.util.HumanReadables
import android.support.v7.widget.RecyclerView
import android.view.View
import org.hamcrest.Matcher
import org.hamcrest.Matchers


object RecyclerViewUtil {

    fun <VH : RecyclerView.ViewHolder> actionOnItemViewAtPosition(position: Int,
                                                                  @IdRes
                                                                  viewId: Int,
                                                                  viewAction: ViewAction): ViewAction {
        return ActionOnItemViewAtPositionViewAction<RecyclerView.ViewHolder>(position, viewId, viewAction)
    }

    class ActionOnItemViewAtPositionViewAction<VH : RecyclerView.ViewHolder>  constructor(private val position: Int,
                                                                                                         @param:IdRes private val viewId: Int,
                                                                                                         private val viewAction: ViewAction) : ViewAction {

        override fun getConstraints(): Matcher<View> {
            return Matchers.allOf(listOf(ViewMatchers.isAssignableFrom(RecyclerView::class.java), ViewMatchers.isDisplayed()))
        }

        override fun getDescription(): String {
            return ("actionOnItemAtPosition performing ViewAction: "
                    + this.viewAction.description
                    + " on item at position: "
                    + this.position)
        }

        override fun perform(uiController: UiController, view: View) {
            val recyclerView = view as RecyclerView
            ScrollToPositionViewAction(this.position).perform(uiController, view)
            uiController.loopMainThreadUntilIdle()

            val targetView = recyclerView.getChildAt(this.position).findViewById<View>(this.viewId)

            if (targetView == null) {
                throw PerformException.Builder().withActionDescription(this.toString())
                        .withViewDescription(

                                HumanReadables.describe(view))
                        .withCause(IllegalStateException(
                                "No view with id "
                                        + this.viewId
                                        + " found at position: "
                                        + this.position))
                        .build()
            } else {
                this.viewAction.perform(uiController, targetView)
            }
        }
    }

    class ScrollToPositionViewAction  constructor(private val position: Int) : ViewAction {

        override fun getConstraints(): Matcher<View> {
            return Matchers.allOf(listOf(ViewMatchers.isAssignableFrom(RecyclerView::class.java), ViewMatchers.isDisplayed()))
        }

        override fun getDescription(): String {
            return "scroll RecyclerView to position: " + this.position
        }

        override fun perform(uiController: UiController, view: View) {
            val recyclerView = view as RecyclerView
            recyclerView.scrollToPosition(this.position)
        }
    }


    fun withRecyclerView(recyclerViewId: Int): RecyclerViewMatcher {

        return RecyclerViewMatcher(recyclerViewId)
    }

}