package com.annanow.android.shop.di

import android.app.Application
import com.annanow.android.shop.TestApp
import com.annanow.android.shop.di.module.ActivityBindingModule
import com.annanow.android.shop.di.module.ServiceModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (AppTestModule::class),
    (ActivityBindingModule::class),
    (AndroidInjectionModule::class),
    (ServiceModule::class)])
interface AppTestComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppTestComponent.Builder

        fun build(): AppTestComponent

    }

    fun inject(testApp: TestApp)

//    TODO add tests
//    fun inject(conversationListDisplayTest: ConversationListDisplayTest)
}